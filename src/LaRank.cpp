/* 
 * Struck: Structured Output Tracking with Kernels
 * 
 * Code to accompany the paper:
 *   Struck: Structured Output Tracking with Kernels
 *   Sam Hare, Amir Saffari, Philip H. S. Torr
 *   International Conference on Computer Vision (ICCV), 2011
 * 
 * Copyright (C) 2011 Sam Hare, Oxford Brookes University, Oxford, UK
 * 
 * This file is part of Struck.
 * 
 * Struck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Struck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Struck.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "LaRank.h"
#include <math.h>
#include "Config.h"
#include "Features.h"
#include "Kernels.h"
#include "Sample.h"
#include "Rect.h"
#include "GraphUtils/GraphUtils.h"
#include <direct.h>
#include <algorithm>
#include <vector>
#include <iterator>
//#include <functional>
#include <Eigen/Array>
#include <time.h>

#include <opencv/highgui.h>
static const int kTileSize = 30;
using namespace cv;

using namespace std;
using namespace Eigen;

static const int kMaxSVs = 2000; // TODO (only used when no budget)

LaRank::LaRank(const Config& conf, const Features& features, const Kernel& kernel, const int _frmInd) :
	m_config(conf),
	m_features(features),
	m_kernel(kernel),
	m_C(conf.svmC),
	m_frmInd(_frmInd)
{
	int N = conf.svmBudgetSize > 0 ? conf.svmBudgetSize+2 : kMaxSVs;
	m_K = MatrixXd::Zero(N, N);
	//m_debugImage = Mat(800, 600, CV_8UC3);
	m_debugImage = Mat(600, 600, CV_8UC3);
	m_score = 1;
	m_startFrmFlag = true;
}

LaRank::~LaRank()
{
}

double LaRank::Evaluate(const Eigen::VectorXd& x, const FloatRect& y) const
{
	double f = 0.0;
	for (int i = 0; i < (int)m_svs.size(); ++i)
	{
		const SupportVector& sv = *m_svs[i];
		f += sv.b*m_kernel.Eval(x, sv.x->x[sv.y]);//y is index, i.e.:sv.x->x[sv.y]=x_{p+y}, Ray
	}
	return f;
}

void LaRank::Eval(const MultiSample& sample, std::vector<double>& results)
{
	unsigned t0=clock(); // for computing time

	const FloatRect& centre(sample.GetRects()[0]);
	vector<VectorXd> fvs;
	const_cast<Features&>(m_features).Eval(sample, fvs);
	results.resize(fvs.size());

	//std::cout << "Frame " << m_frmInd << " MultiSample " << sample.GetRects().size() << " costs "<<(double)(clock() - t0)/CLOCKS_PER_SEC << " seconds."<< std::endl;
	
	unsigned t1=clock(); // for computing time

	for (int i = 0; i < (int)fvs.size(); ++i)
	{
		// express y in coord frame of centre sample
		FloatRect y(sample.GetRects()[i]);
		y.Translate(-centre.XMin(), -centre.YMin());
		results[i] = Evaluate(fvs[i], y);
	}

	//std::cout << "Frame " << m_frmInd << " MultiSample Evaluate costs "<<(double)(clock() - t1)/CLOCKS_PER_SEC << " seconds."<< std::endl;
}

void LaRank::Eval(const Sample& x, double& result)
{
	VectorXd fv = const_cast<Features&>(m_features).Eval(x);
	FloatRect y(x.GetROI());
	result = Evaluate(fv, y);
}

void LaRank::Update(const MultiSample& sample, int y)
{
	// add new support pattern
	SupportPattern* sp = new SupportPattern;
	const vector<FloatRect>& rects = sample.GetRects();

	m_keys.clear();

	FloatRect centre = rects[y];
	m_centerRect = centre;
	double maxKey = -DBL_MAX;
	int maxLoc = 0;

	for (int i = 0; i < (int)rects.size(); ++i)
	{
		// express r in coord frame of centre sample
		FloatRect r = rects[i];
		r.Translate(-centre.XMin(), -centre.YMin());
		sp->yv.push_back(r);

		double weight = (100 - (abs(r.XMin()) + abs(r.YMin())))/10.0;
		sp->weights.push_back( weight * pow((double)1.5,m_frmInd+1) ); 

		double n_rand = (double)rand()/(double)RAND_MAX;
		sp->keys.push_back( n_rand * sp->weights[i] );

		if(maxKey < sp->keys[i])
		{
			maxKey = sp->keys[i];
			maxLoc = i;
		}
		
		if (!m_config.quietMode && m_config.debugMode)
		{
			// store a thumbnail for each sample
			Mat im(kTileSize, kTileSize, CV_8UC1);
			IntRect rect = rects[i];
			cv::Rect roi(rect.XMin(), rect.YMin(), rect.Width(), rect.Height());
			cv::resize(sample.GetImage().GetImage(0)(roi), im, im.size());
			sp->images.push_back(im);
		}
	}

	// make sure that the first patch will get max Key
	double tmpKey = sp->keys[0];
	sp->keys[0] = sp->keys[maxLoc];
	sp->keys[maxLoc] = tmpKey;

	// evaluate features for each sample
	sp->x.resize(rects.size());
	const_cast<Features&>(m_features).Eval(sample, sp->x);
	sp->y = y;
	sp->refCount = 0;
	sp->frmInd_1 = m_frmInd;

	// Edit by Rui Yao, 2011-12-28
	UpdateWeightedReservoir(sp);
	std::vector<int> ind_delete;
	int size_delete = 0;
	
	std::vector<int> randomExampleOrder;
	for (int i = 0; i < (int)m_R.sps.size(); ++i) 
	{
		//reverse
		randomExampleOrder.push_back(m_R.sps.size()-1-i);
	}
	// training examples randomly 
	//random_shuffle ( randomExampleOrder.begin(), randomExampleOrder.end() );
	for (std::vector<int>::iterator it = randomExampleOrder.begin(); it != randomExampleOrder.end(); ++it)
	{
		int num = *it;
		m_sps.push_back(m_R.sps[num]);

		ProcessNew((int)m_sps.size()-1);
		BudgetMaintenance();
	
		// unsigned t0=clock(); // for computing time
		for (int j = 0; j < 10; ++j)
		{
			Reprocess();
			BudgetMaintenance();
		}
		// std::cout << "Frame " << m_frmInd << "Reprocess" << " costs "<<(double)(clock() - t0)/CLOCKS_PER_SEC << " seconds."<< std::endl;
	}

	m_R.size = 0;
	std::vector<int> deleteInd;

	for (int i = 0; i < (int)m_R.sps.size(); i++)
	{
		if(m_R.sps[i] == NULL)
		{
			deleteInd.push_back(i);
		}
		else
		{
			m_R.size += m_R.sps[i]->yv.size();
		}
	}
	for (std::vector<int>::iterator iter = deleteInd.begin(); iter < deleteInd.end(); iter++)
	{
		m_R.sps.erase(m_R.sps.begin()+*iter);
		std::cout << "m_R.sps.erase" << std::endl;
	}
}

void LaRank::BudgetMaintenance()
{
	if (m_config.svmBudgetSize > 0)
	{
		while ((int)m_svs.size() > m_config.svmBudgetSize)
		{
			BudgetMaintenanceRemove();
		}
	}
}

void LaRank::Reprocess()
{
	ProcessOld();
	for (int i = 0; i < 10; ++i)
	{
		Optimize();
	}
}

double LaRank::ComputeDual() const
{
	double d = 0.0;
	for (int i = 0; i < (int)m_svs.size(); ++i)
	{
		const SupportVector* sv = m_svs[i];
		d -= sv->b*Loss(sv->x->yv[sv->y], sv->x->yv[sv->x->y]);
		for (int j = 0; j < (int)m_svs.size(); ++j)
		{
			d -= 0.5*sv->b*m_svs[j]->b*m_K(i,j);
		}
	}
	return d;
}

void LaRank::SMOStep(int ipos, int ineg)
{
	if (ipos == ineg) return;

	SupportVector* svp = m_svs[ipos];
	SupportVector* svn = m_svs[ineg];
	assert(svp->x == svn->x);
	SupportPattern* sp = svp->x;

#if VERBOSE
	cout << "SMO: gpos:" << svp->g << " gneg:" << svn->g << endl;
#endif	
	if ((svp->g - svn->g) < 1e-5)
	{
#if VERBOSE
		cout << "SMO: skipping" << endl;
#endif		
	}
	else
	{
		double kii = m_K(ipos, ipos) + m_K(ineg, ineg) - 2*m_K(ipos, ineg);
		double lu = (svp->g-svn->g)/kii;
		// no need to clamp against 0 since we'd have skipped in that case
		double l = min(lu, m_C*(int)(svp->y == sp->y) - svp->b);

		svp->b += l;
		svn->b -= l;

		// update gradients
		for (int i = 0; i < (int)m_svs.size(); ++i)
		{
			SupportVector* svi = m_svs[i];
			svi->g -= l*(m_K(i, ipos) - m_K(i, ineg));
		}
#if VERBOSE
		cout << "SMO: " << ipos << "," << ineg << " -- " << svp->b << "," << svn->b << " (" << l << ")" << endl;
#endif		
	}
	
	// check if we should remove either sv now
	
	if (fabs(svp->b) < 1e-8)
	{
		RemoveSupportVector(ipos);
		if (ineg == (int)m_svs.size())
		{
			// ineg and ipos will have been swapped during sv removal
			ineg = ipos;
		}
	}

	if (fabs(svn->b) < 1e-8)
	{
		RemoveSupportVector(ineg);
	}
}

pair<int, double> LaRank::MinGradient(int ind)
{
	const SupportPattern* sp = m_sps[ind];
	pair<int, double> minGrad(-1, DBL_MAX);
	for (int i = 0; i < (int)sp->yv.size(); ++i)
	{
		double grad = -Loss(sp->yv[i], sp->yv[sp->y]) - Evaluate(sp->x[i], sp->yv[i]);
		if (grad < minGrad.second)
		{
			minGrad.first = i;
			minGrad.second = grad;
		}
	}
	return minGrad;
}

void LaRank::ProcessNew(int ind)
{
	// gradient is -f(x,y) since loss=0
	int ip = AddSupportVector(m_sps[ind], m_sps[ind]->y, -Evaluate(m_sps[ind]->x[m_sps[ind]->y],m_sps[ind]->yv[m_sps[ind]->y]));

	pair<int, double> minGrad = MinGradient(ind);
	int in = AddSupportVector(m_sps[ind], minGrad.first, minGrad.second);

	SMOStep(ip, in);
}

void LaRank::ProcessOld()
{
	if (m_sps.size() == 0) return;

	// choose pattern to process
	int ind = rand() % m_sps.size();

	// find existing sv with largest grad and nonzero beta
	int ip = -1;
	double maxGrad = -DBL_MAX;
	for (int i = 0; i < (int)m_svs.size(); ++i)
	{
		if (m_svs[i]->x != m_sps[ind]) continue;

		const SupportVector* svi = m_svs[i];
		if (svi->g > maxGrad && svi->b < m_C*(int)(svi->y == m_sps[ind]->y))
		{
			ip = i;
			maxGrad = svi->g;
		}
	}
	assert(ip != -1);
	if (ip == -1) return;

	// find potentially new sv with smallest grad
	pair<int, double> minGrad = MinGradient(ind);
	int in = -1;
	for (int i = 0; i < (int)m_svs.size(); ++i)
	{
		if (m_svs[i]->x != m_sps[ind]) continue;

		if (m_svs[i]->y == minGrad.first)
		{
			in = i;
			break;
		}
	}
	if (in == -1)
	{
		// add new sv
		in = AddSupportVector(m_sps[ind], minGrad.first, minGrad.second);
	}
	//if(ip==in)
	//	cout << "processOld(): ip=in " << endl;

	SMOStep(ip, in);
}

void LaRank::Optimize()
{
	if (m_sps.size() == 0) return;
	
	// choose pattern to optimize
	int ind = rand() % m_sps.size();

	int ip = -1;
	int in = -1;
	double maxGrad = -DBL_MAX;
	double minGrad = DBL_MAX;
	for (int i = 0; i < (int)m_svs.size(); ++i)
	{
		if (m_svs[i]->x != m_sps[ind]) continue;

		const SupportVector* svi = m_svs[i];
		if (svi->g > maxGrad && svi->b < m_C*(int)(svi->y == m_sps[ind]->y))
		{
			ip = i;
			maxGrad = svi->g;
		}
		if (svi->g < minGrad)
		{
			in = i;
			minGrad = svi->g;
		}
	}
	assert(ip != -1 && in != -1);
	if (ip == -1 || in == -1)
	{
		// this shouldn't happen
		cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
		return;
	}
	//if(ip==in)
	//	cout << "Optimize(): ip=in " << endl;

	SMOStep(ip, in);
}

int LaRank::AddSupportVector(SupportPattern* x, int y, double g)
{
	SupportVector* sv = new SupportVector;
	sv->b = 0.0;
	sv->x = x;
	sv->y = y;
	sv->g = g;

	int ind = (int)m_svs.size();
	m_svs.push_back(sv);
	x->refCount++;

#if VERBOSE
	cout << "Adding SV: " << ind << endl;
#endif

	// update kernel matrix
	for (int i = 0; i < ind; ++i)
	{
		m_K(i,ind) = m_kernel.Eval(m_svs[i]->x->x[m_svs[i]->y], x->x[y]);
		m_K(ind,i) = m_K(i,ind);
	}
	m_K(ind,ind) = m_kernel.Eval(x->x[y]);

	return ind;
}

void LaRank::SwapSupportVectors(int ind1, int ind2)
{
	SupportVector* tmp = m_svs[ind1];
	m_svs[ind1] = m_svs[ind2];
	m_svs[ind2] = tmp;
	
	VectorXd row1 = m_K.row(ind1);
	m_K.row(ind1) = m_K.row(ind2);
	m_K.row(ind2) = row1;
	
	VectorXd col1 = m_K.col(ind1);
	m_K.col(ind1) = m_K.col(ind2);
	m_K.col(ind2) = col1;
}

void LaRank::RemoveSupportVector(int ind)
{
#if VERBOSE
	cout << "Removing SV: " << ind << endl;
#endif	

	m_svs[ind]->x->refCount--;
	if (m_svs[ind]->x->refCount == 0)
	{
		// also remove the support pattern
		for (int i = 0; i < (int)m_sps.size(); ++i)
		{
			if (m_sps[i] == m_svs[ind]->x)
			{
				//delete m_sps[i];
				m_sps[i] = NULL;
				delete m_sps[i];
				m_sps.erase(m_sps.begin()+i);
				break;
			}
		}
	}

	// make sure the support vector is at the back, this
	// lets us keep the kernel matrix cached and valid
	if (ind < (int)m_svs.size()-1)
	{
		SwapSupportVectors(ind, (int)m_svs.size()-1);
		ind = (int)m_svs.size()-1;
	}
	delete m_svs[ind];
	m_svs.pop_back();
}

double LaRank::F_K(double k)
{
	//double val = m_svMergeM * (m_svMergeKernel^((1 - k)^2)) + (1 - m_svMergeM) * (m_svMergeKernel^(k^2));
	double val = m_svMergeM * ( pow(m_svMergeKernel, pow(1 - k, 2)) ) + (1 - m_svMergeM) * ( pow(m_svMergeKernel, pow(k, 2)) );
	return -val;
}

/**
 * Rui Yao
 * Find minimum value of F_K
 * a and c are the current bounds; the minimum is between them.
 * b is a center point
 * tau is a tolerance parameter of the algorithm
 * F_K is some mathematical function elsewhere defined
*/
double LaRank::GoldenSectionSearch(double a, double b, double c, double tau)
{
	double pow_val = std::pow(5.0, 0.5);
	double phi = (1 + pow(5, 0.5)) / 2;
	double resphi = 2 - phi;
	double x = 0.0;

	//std::cout<< "a = " << a << std::endl; 

	if (c - b > b - a)
		x = b + resphi * (c - b);
	else
		x = b - resphi * (b - a);

	if (abs(c - a) < tau * (abs(b) + abs(x))) 
		return (c + a) / 2; 

	if (F_K(x) < F_K(b)) 
		if (c - b > b - a) 
			return GoldenSectionSearch(b, x, c, tau);
		else
			return GoldenSectionSearch(a, x, b, tau);
	else 
		if (c - b > b - a) 
			return GoldenSectionSearch(a, b, x, tau);
		else
			return GoldenSectionSearch(x, b, c, tau);
}

void LaRank::BudgetMaintenanceRemove()
{
	// find negative sv with smallest effect on discriminant function if removed
	double minVal = DBL_MAX;
	int in = -1;
	int ip = -1;
	for (int i = 0; i < (int)m_svs.size(); ++i)
	{
		if (m_svs[i]->b < 0.0)
		{
			// find corresponding positive sv
			int j = -1;
			for (int k = 0; k < (int)m_svs.size(); ++k)
			{
				if (m_svs[k]->b > 0.0 && m_svs[k]->x == m_svs[i]->x)
				{
					j = k;
					break;
				}
			}
			double val = m_svs[i]->b*m_svs[i]->b*(m_K(i,i) + m_K(j,j) - 2.0*m_K(i,j));
			if (val < minVal)
			{
				minVal = val;
				in = i;
				ip = j;
			}
		}
	}
	SupportPattern* sp = m_svs[in]->x;

	// adjust weight of positive sv to compensate for removal of negative
	m_svs[ip]->b += m_svs[in]->b;

	// remove negative sv
	RemoveSupportVector(in);
	if (ip == (int)m_svs.size())
	{
		// ip and in will have been swapped during support vector removal
		ip = in;
	}
	
	if (m_svs[ip]->b < 1e-8)
	{
		// also remove positive sv
		RemoveSupportVector(ip);
	}

	// update gradients
	// TODO: this could be made cheaper by just adjusting incrementally rather than recomputing
	for (int i = 0; i < (int)m_svs.size(); ++i)
	{
		SupportVector& svi = *m_svs[i];
		svi.g = -Loss(svi.x->yv[svi.y],svi.x->yv[svi.x->y]) - Evaluate(svi.x->x[svi.y], svi.x->yv[svi.y]);
	}	
}

void LaRank::Debug()
{
	cout << m_sps.size() << "/" << m_svs.size() << " support patterns/vectors" << endl;
	UpdateDebugImage();
	imshow("learner", m_debugImage);
}

void LaRank::UpdateDebugImage()
{
	m_debugImage.setTo(0);
	
	int n = (int)m_svs.size();
	
	if (n == 0) return;
	
	const int kCanvasSize = 600;
	int gridSize = (int)sqrtf((float)(n-1)) + 1;
	int tileSize = (int)((float)kCanvasSize/gridSize);
	
	if (tileSize < 5)
	{
		cout << "too many support vectors to display" << endl;
		return;
	}
	
	Mat temp(tileSize, tileSize, CV_8UC1);
	int x = 0;
	int y = 0;
	int ind = 0;
	float vals[kMaxSVs];
	memset(vals, 0, sizeof(float)*n);
	int drawOrder[kMaxSVs];

	for (int set = 0; set < 2; ++set)
	{
		for (int i = 0; i < n; ++i)
		{
			if (((set == 0) ? 1 : -1)*m_svs[i]->b < 0.0) continue;
			
			drawOrder[ind] = i;
			vals[ind] = (float)m_svs[i]->b;
			++ind;
			
			Mat I = m_debugImage(cv::Rect(x, y, tileSize, tileSize));
			resize(m_svs[i]->x->images[m_svs[i]->y], temp, temp.size());
			cvtColor(temp, I, CV_GRAY2RGB);
			double w = 1.0;
			rectangle(I, Point(0, 0), Point(tileSize-1, tileSize-1), (m_svs[i]->b > 0.0) ? CV_RGB(0, (uchar)(255*w), 0) : CV_RGB((uchar)(255*w), 0, 0), 3);
			x += tileSize;
			if ((x+tileSize) > kCanvasSize)
			{
				y += tileSize;
				x = 0;
			}
		}
	}
	
}

void LaRank::SaveSV(int frmInd)
{
	// support vector
	char svPath[256];
	std::string svFormat = m_config.sequenceBasePath+"/"+m_config.sequenceName+"/scores/%d_sv_loc.txt";
	sprintf(svPath, svFormat.c_str(), frmInd);
	std::fstream svFile(svPath, ios::out);	
	
	if(svFile.is_open())
	{
		for (int i = 0; i < (int)m_svs.size(); i++)
		{
			FloatRect y_rect = m_svs[i]->x->yv[m_svs[i]->y];
			y_rect.Translate(m_centerRect.XMin(), m_centerRect.YMin());
			svFile << y_rect.XMin() << "," << y_rect.YMin() << "," << y_rect.Width() << "," << y_rect.Height() << std::endl;
		}
		svFile.close();
	}
}

void LaRank::UpdateWeightedReservoir(SupportPattern* sp)
{
	// 1. First m item of v are inserted into R
	WeightedReservoir tmpR;
	
	// 2. Update the key of all training examples
	for (int i = 0; i < (int)m_R.sps.size(); i++)
	{
		double maxKey = -DBL_MAX;
		int maxLoc = 0;

		for (int j = 0; j < (int)m_R.sps[i]->yv.size(); j++)
		{

			double n_rand = (double)rand()/(double)RAND_MAX;

			m_R.sps[i]->keys[j] = n_rand * m_R.sps[i]->weights[j];
			
			m_keys.push_back(m_R.sps[i]->keys[j]);
			if(maxKey < m_R.sps[i]->keys[j])
			{
				maxKey = m_R.sps[i]->keys[j];
				maxLoc = j;
			}
			//std::cout << m_R.sps[i]->keys[j] << std::endl;
		}

		// make sure that the first patch will get max Key
		double tmpKey = m_R.sps[i]->keys[0];
		m_R.sps[i]->keys[0] = m_R.sps[i]->keys[maxLoc];
		m_R.sps[i]->keys[maxLoc] = tmpKey;
	}

	tmpR.capacity = m_config.WRCapacity;

	tmpR.size = sp->yv.size();
	tmpR.sps.push_back(sp);

	double threshold_key = -DBL_MAX;
	std::sort(m_keys.begin(), m_keys.end()); // from minimum to maximum
	// 3. Sort the key of all training examples and find the threshold
	if(tmpR.capacity-tmpR.size > 0)
	{
		if ((int)m_keys.size() > (tmpR.capacity-tmpR.size))
		{
			int k_ind = m_keys.size() - (tmpR.capacity-tmpR.size);
			if ( k_ind > 0)
			{
				threshold_key = m_keys[ k_ind ]; // maybe +1
			}
		}
	}

	// 4. Restore new Support Patterns
	for (int i = 0; i < (int)m_R.sps.size(); i++)
	{
		SupportPattern* newSp = new SupportPattern;
		int flag = 0;
		for (int j = 0; j < (int)m_R.sps[i]->yv.size(); j++)
		{
			if (m_R.sps[i]->keys[j] >= threshold_key)
			{
				newSp->x.push_back(m_R.sps[i]->x[j]);
				newSp->yv.push_back(m_R.sps[i]->yv[j]);
				newSp->weights.push_back(m_R.sps[i]->weights[j]);
				newSp->keys.push_back(m_R.sps[i]->keys[j]);
				
				if (!m_config.quietMode && m_config.debugMode)
				{
					newSp->images.push_back(m_R.sps[i]->images[j]);
				}
				flag = 1;
			}
		}
		if (flag == 1)
		{
			newSp->y = m_R.sps[i]->y;
			newSp->refCount = 0;
			newSp->frmInd_1 = m_R.sps[i]->frmInd_1;
			tmpR.sps.push_back(newSp);
			tmpR.size += newSp->x.size();
		}
	}

	// 5. instead of weighted reservior
	m_R.sps.clear();
	m_R = tmpR;
}

void LaRank::ClearAllSp()
{
	m_AllSp.x.clear();
	m_AllSp.yv.clear();
	m_AllSp.keys.clear();
	m_AllSp.weights.clear();
	m_AllSp.refCount = 0;
	m_AllSp.images.clear();
}