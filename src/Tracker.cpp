/* 
* Struck: Structured Output Tracking with Kernels
* 
* Code to accompany the paper:
*   Struck: Structured Output Tracking with Kernels
*   Sam Hare, Amir Saffari, Philip H. S. Torr
*   International Conference on Computer Vision (ICCV), 2011
* 
* Copyright (C) 2011 Sam Hare, Oxford Brookes University, Oxford, UK
* 
* This file is part of Struck.
* 
* Struck is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* Struck is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with Struck.  If not, see <http://www.gnu.org/licenses/>.
* 
*/

#include "Tracker.h"
#include "Config.h"
#include "ImageRep.h"
#include "Sampler.h"
#include "Sample.h"
#include "GraphUtils/GraphUtils.h"

#include "HaarFeatures.h"
#include "RawFeatures.h"
#include "HistogramFeatures.h"
#include "MultiFeatures.h"

#include "Kernels.h"

#include "LaRank.h"

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <Eigen/Core>

#include <vector>
#include <algorithm>
#include <iostream>
#include <fstream>

#include <time.h>

#define _USE_MATH_DEFINES
#include <cmath>

#ifndef M_PI
#define M_PI 3.1415926
#endif

using namespace cv;
using namespace std;
using namespace Eigen;

Tracker::Tracker(const Config& conf) :
m_config(conf),
m_initialised(false),
m_pLearner(0),
m_debugImage(2*conf.searchRadius+1, 2*conf.searchRadius+1, CV_32FC1),
m_needsIntegralImage(false),
m_frmInd(0)
{
	Reset();
}

Tracker::~Tracker()
{
	delete m_pLearner;
	for (int i = 0; i < (int)m_features.size(); ++i)
	{
		delete m_features[i];
		delete m_kernels[i];
	}
}

void Tracker::Reset()
{
	m_initialised = false;
	m_debugImage.setTo(0);
	if (m_pLearner) delete m_pLearner;
	for (int i = 0; i < (int)m_features.size(); ++i)
	{
		delete m_features[i];
		delete m_kernels[i];
	}
	m_features.clear();
	m_kernels.clear();

	m_needsIntegralImage = false;
	m_needsIntegralHist = false;

	int numFeatures = m_config.features.size();
	vector<int> featureCounts;
	for (int i = 0; i < numFeatures; ++i)
	{
		switch (m_config.features[i].feature)
		{
		case Config::kFeatureTypeHaar:
			m_features.push_back(new HaarFeatures(m_config));
			m_needsIntegralImage = true;
			break;			
		case Config::kFeatureTypeRaw:
			m_features.push_back(new RawFeatures(m_config));
			break;
		case Config::kFeatureTypeHistogram:
			m_features.push_back(new HistogramFeatures(m_config));
			m_needsIntegralHist = true;
			break;
		}
		featureCounts.push_back(m_features.back()->GetCount());

		switch (m_config.features[i].kernel)
		{
		case Config::kKernelTypeLinear:
			m_kernels.push_back(new LinearKernel());
			break;
		case Config::kKernelTypeGaussian:
			m_kernels.push_back(new GaussianKernel(m_config.features[i].params[0]));
			break;
		case Config::kKernelTypeIntersection:
			m_kernels.push_back(new IntersectionKernel());
			break;
		case Config::kKernelTypeChi2:
			m_kernels.push_back(new Chi2Kernel());
			break;
		}
	}

	if (numFeatures > 1)
	{
		MultiFeatures* f = new MultiFeatures(m_features);
		m_features.push_back(f);

		MultiKernel* k = new MultiKernel(m_kernels, featureCounts);
		m_kernels.push_back(k);		
	}
		
	m_pLearner = new LaRank(m_config, *m_features.back(), *m_kernels.back(), m_frmInd);
}


void Tracker::Initialise(const cv::Mat& frame, FloatRect bb)
{
	m_bb = IntRect(bb);

	ImageRep image(frame, m_needsIntegralImage, m_needsIntegralHist);
	for (int i = 0; i < 1; ++i)
	{
		UpdateLearner(image);
	}
	m_initialised = true;
}

// added by Rui Yao, 2011-12-15
void Tracker::SaveScore(std::vector<double> scores, std::vector<FloatRect>& keptRects)
{
	char scoresPath[256];
	std::string scoresFormat = m_config.sequenceBasePath+"/"+m_config.sequenceName+"/scores/%d.txt";
	sprintf(scoresPath, scoresFormat.c_str(), m_frmInd);
	std::fstream scoresFile(scoresPath, ios::out);	

	if(scoresFile.is_open())
	{
		scoresFile << m_pre_bb.XMin() << "," << m_pre_bb.YMin() << std::endl;
		for (int i = 0; i < (int)scores.size(); i++)
		{
			scoresFile << keptRects[i].XMin() << "," << keptRects[i].YMin() << ":" << scores[i] << std::endl;
		}
		scoresFile.close();
	}
}

void Tracker::Track(const cv::Mat& frame)
{
	assert(m_initialised);

	ImageRep image(frame, m_needsIntegralImage, m_needsIntegralHist);

	if (m_config.greedySearchPoints == 0)
		FullSearchEval(image);
	else
		GreedySearchEval(image);
	//PFEval(im6age);
}

void Tracker::GreedySearchEval(const ImageRep& image)
{
	IntRect startPos = m_bb;
	m_pbFlag = MatrixXd::Zero(2*m_config.searchRadius, 2*m_config.searchRadius);
	m_iPMX = startPos.XMin() - m_config.searchRadius; 
	m_iPMY = startPos.YMin() - m_config.searchRadius; 

	//m_EvalCount = 0;
	m_bb = KNNSearchEval(image, m_config.greedySearchPoints);

	UpdateLearner(image);
	//std::cout << "m_EvalCount = " << m_EvalCount << std::endl;
}

FloatRect Tracker::ScaleEval(const ImageRep& image, int n_scales)
{
	double maxScore = m_pLearner->m_score;
	double scale[] = {1.1, 1.2, 0.95, 0.9, 0.85, 0.8, 0.75, 0.7, 0.65, 0.6};
	FloatRect finalRect = m_bb;
	for(int i = 1; i <= n_scales; i++)
	{
		FloatRect rect(m_bb.XCentre()-(scale[i-1]*m_bb.Width())/2,
			m_bb.YCentre()-(scale[i-1]*m_bb.Height())/2,
			scale[i-1]*m_bb.Width(), scale[i-1]*m_bb.Height()); 

		// std::cout << rect.XMin() << ", " << rect.YMin() << ", " << rect.Width() << ", " << rect.Height() << std::endl;

		if (!rect.IsInside(image.GetRect())) continue;

		double tmpScore = EvalTestRect(image, rect);
		if(tmpScore > maxScore)
		{
			finalRect = rect;
			maxScore = tmpScore;
		}
	}

	// std::cout << finalRect.XMin() << ", " << finalRect.YMin() << ", " << finalRect.Width() << ", " << finalRect.Height() << std::endl;

	m_pLearner->m_score = maxScore;
	return finalRect;
}

void Tracker::FullSearchEval(const ImageRep& image)
{
	m_EvalCount = 0;
	vector<FloatRect> rects = Sampler::PixelSamples(m_bb, m_config.searchRadius); 

	vector<FloatRect> keptRects;
	keptRects.reserve(rects.size());
	for (int i = 0; i < (int)rects.size(); ++i)
	{
		if (!rects[i].IsInside(image.GetRect())) continue;
		keptRects.push_back(rects[i]);
	}

	m_EvalCount = keptRects.size();
	//std::cout << "m_EvalCount = " << m_EvalCount << std::endl;

	MultiSample sample(image, keptRects);

	vector<double> scores;
	m_pLearner->Eval(sample, scores);

	double bestScore = -DBL_MAX;
	int bestInd = -1;
	for (int i = 0; i < (int)keptRects.size(); ++i)
	{		
		if (scores[i] > bestScore)
		{
			bestScore = scores[i];
			bestInd = i;
		}
	}

	UpdateDebugImage(keptRects, m_bb, scores);

	if (bestInd != -1)
	{
		m_bb = keptRects[bestInd];
		m_pLearner->m_score = bestScore;
		//// save scores
		SaveScore(scores, keptRects);
		//// save support vector
		m_pLearner->SaveSV( m_frmInd );

		UpdateLearner(image);
		m_pre_bb = m_bb;
#if VERBOSE		
		cout << "track score: " << bestScore << endl;
#endif
	}
}

void Tracker::UpdateDebugImage(const vector<FloatRect>& samples, const FloatRect& centre, const vector<double>& scores)
{
	//std::cout << "Be Not Called!" << std::endl;
	double mn = VectorXd::Map(&scores[0], scores.size()).minCoeff();
	double mx = VectorXd::Map(&scores[0], scores.size()).maxCoeff();
	m_debugImage.setTo(0);
	for (int i = 0; i < (int)samples.size(); ++i)
	{
		int x = (int)(samples[i].XMin() - centre.XMin());
		int y = (int)(samples[i].YMin() - centre.YMin());
		m_debugImage.at<float>(m_config.searchRadius+y, m_config.searchRadius+x) = (float)((scores[i]-mn)/(mx-mn));
	}
}

void Tracker::Debug()
{
	// imshow("tracker", m_debugImage); // show scores in search circles
	m_pLearner->Debug();
}

void Tracker::UpdateLearner(const ImageRep& image)
{
	// note these return the centre sample at index 0
	vector<FloatRect> rects = Sampler::RadialSamples(m_bb, 2*m_config.searchRadius, 5, 16);

	vector<FloatRect> keptRects;
	keptRects.push_back(rects[0]); // the true sample
	for (int i = 1; i < (int)rects.size(); ++i)
	{
		if (!rects[i].IsInside(image.GetRect())) continue;
		keptRects.push_back(rects[i]);
	}

#if VERBOSE		
	cout << keptRects.size() << " samples" << endl;
#endif

	MultiSample sample(image, keptRects);
	m_pLearner->m_frmInd = m_frmInd; // VIP
	m_pLearner->Update(sample, 0);
}

double Tracker::EvalTestRect(const ImageRep& image, const FloatRect& rect)
{
	//unsigned t0=clock(); // for computing time

	Sample objSample(image, rect);
	double objScore = -DBL_MAX;
	m_pLearner->Eval(objSample, objScore);

	//std::cout << "Frame " << m_frmInd << " Eval 1 sample costs " << clock() << " - " << t0 << "/1000 second." << std::endl;

	return objScore;
}

FloatRect Tracker::KNNSearchEval(const ImageRep& image, int N)
{
	bool isSucceed = true;
	double finalObjScore = -DBL_MAX;
	FloatRect finalObjRect = EightNeighborSearchEval( m_bb, image, finalObjScore, isSucceed );

	double circleStep = 2 * M_PI / N;
	int searchStep = 10;
	int innerLoopNum = N/searchStep;
	//int innerLoopNum = 2;
	int npFlag = -1;
		
	// divide search circle by N
	for(int k = 0; k < N; k++)
	{
		for(int j = 1; j < innerLoopNum; j ++)
		{
			double tmpObjScore = -DBL_MAX;
			FloatRect tmpRect = m_bb;

			double cosP = npFlag * cos(M_PI + k * circleStep + ( (j-1)*M_PI/3 )); // (j-1)*M_PI/3 is offset
			double sinP = npFlag * sin(M_PI + k * circleStep + ( (j-1)*M_PI/3 ));
			int xT = (int)(cosP * searchStep * j);
			int yT = (int)(sinP * searchStep * j);

			tmpRect.Translate((float)xT, (float)yT);
			if(!tmpRect.IsInside(image.GetRect()))
				continue;

			isSucceed = true;
			FloatRect tmpObjRect = EightNeighborSearchEval( tmpRect, image, tmpObjScore, isSucceed );
			if( isSucceed )
			{
				if(tmpObjScore > finalObjScore)
				{
					finalObjScore = tmpObjScore;
					finalObjRect = tmpObjRect;
				}
			}
		}
	}

	m_maxPredictScore.push_back(finalObjScore);

	m_pLearner->m_score = finalObjScore;

	return finalObjRect;
}

FloatRect Tracker::EightNeighborSearchEval(FloatRect initBB, const ImageRep& image, double& score, bool& isSucceed)
{
	IntRect startPos = initBB;
	IntRect objPos = initBB;
	double startScore = EvalTestRect(image, startPos);
	double objScore = startScore;

	int direction[8][2] = {{-1,-1},{0,-1},{1,-1},{-1,0},{1,0},{-1,1},{0,1},{1,1}};
	//int direction[8][2] = {{-3,-3},{0,-3},{3,-3},{-3,0},{3,0},{-3,3},{0,3},{3,3}};

	while(1)
	{
		IntRect tmp = startPos;
		// left, up
		tmp.Translate(direction[0][0], direction[0][1]);
		if( tmp.IsInside(image.GetRect()) )
		{
			if( m_pbFlag(tmp.XMin()-m_iPMX,tmp.YMin()-m_iPMY) == 0 )	
			{
				m_EvalCount++;
				double tmpScore = EvalTestRect(image, tmp);
				if(tmpScore > objScore)
				{
					objPos = tmp;
					objScore = tmpScore;
				}
				m_pbFlag(tmp.XMin()-m_iPMX,tmp.YMin()-m_iPMY) = 1;
			}
		}

		// center, up
		tmp = startPos;
		tmp.Translate(direction[1][0], direction[1][1]);
		if( tmp.IsInside(image.GetRect()) )
		{
			if( m_pbFlag(tmp.XMin()-m_iPMX,tmp.YMin()-m_iPMY) == 0 )	
			{
				m_EvalCount++;
				double tmpScore = EvalTestRect(image, tmp);
				if(tmpScore > objScore)
				{
					objPos = tmp;
					objScore = tmpScore;
				}
				m_pbFlag(tmp.XMin()-m_iPMX,tmp.YMin()-m_iPMY) = 1;
			}
		}

		// right, up
		tmp = startPos;
		tmp.Translate(direction[2][0], direction[2][1]);
		if( tmp.IsInside(image.GetRect()) )
		{
			if( m_pbFlag(tmp.XMin()-m_iPMX,tmp.YMin()-m_iPMY) == 0  )	
			{
				m_EvalCount++;
				double tmpScore = EvalTestRect(image, tmp);
				if(tmpScore > objScore)
				{
					objPos = tmp;
					objScore = tmpScore;
				}
				m_pbFlag(tmp.XMin()-m_iPMX,tmp.YMin()-m_iPMY) = 1;
			}
		}

		// left, center
		tmp = startPos;
		tmp.Translate(direction[3][0], direction[3][1]);
		if( tmp.IsInside(image.GetRect()) )
		{
			if( m_pbFlag(tmp.XMin()-m_iPMX,tmp.YMin()-m_iPMY) == 0 )	
			{
				m_EvalCount++;
				double tmpScore = EvalTestRect(image, tmp);
				if(tmpScore > objScore)
				{
					objPos = tmp;
					objScore = tmpScore;
				}
				m_pbFlag(tmp.XMin()-m_iPMX,tmp.YMin()-m_iPMY) = 1;
			}
		}

		// right, center
		tmp = startPos;
		tmp.Translate(direction[4][0], direction[4][1]);
		if( tmp.IsInside(image.GetRect()) )
		{
			if( m_pbFlag(tmp.XMin()-m_iPMX,tmp.YMin()-m_iPMY) == 0 )	
			{
				m_EvalCount++;
				double tmpScore = EvalTestRect(image, tmp);
				if(tmpScore > objScore)
				{
					objPos = tmp;
					objScore = tmpScore;
				}
				m_pbFlag(tmp.XMin()-m_iPMX,tmp.YMin()-m_iPMY) = 1;
			}
		}

		// left, below
		tmp = startPos;
		tmp.Translate(direction[5][0], direction[5][1]);
		if( tmp.IsInside(image.GetRect()) )
		{
			if( m_pbFlag(tmp.XMin()-m_iPMX,tmp.YMin()-m_iPMY) == 0 )	
			{
				m_EvalCount++;
				double tmpScore = EvalTestRect(image, tmp);
				if(tmpScore > objScore)
				{
					objPos = tmp;
					objScore = tmpScore;
				}
				m_pbFlag(tmp.XMin()-m_iPMX,tmp.YMin()-m_iPMY) = 1;
			}
		}

		// center, below
		tmp = startPos;
		tmp.Translate(direction[6][0], direction[6][1]);
		if( tmp.IsInside(image.GetRect()) )
		{
			if( m_pbFlag(tmp.XMin()-m_iPMX,tmp.YMin()-m_iPMY) == 0 )	
			{
				m_EvalCount++;
				double tmpScore = EvalTestRect(image, tmp);
				if(tmpScore > objScore)
				{
					objPos = tmp;
					objScore = tmpScore;
				}
				m_pbFlag(tmp.XMin()-m_iPMX,tmp.YMin()-m_iPMY) = 1;
			}
		}
		// right, below
		tmp = startPos;
		tmp.Translate(direction[7][0], direction[7][1]);
		if( tmp.IsInside(image.GetRect()) )
		{
			if( m_pbFlag(tmp.XMin()-m_iPMX,tmp.YMin()-m_iPMY) == 0 )	
			{
				m_EvalCount++;
				double tmpScore = EvalTestRect(image, tmp);
				if(tmpScore > objScore)
				{
					objPos = tmp;
					objScore = tmpScore;
				}
				m_pbFlag(tmp.XMin()-m_iPMX,tmp.YMin()-m_iPMY) = 1;
			}
		}

		if( objScore == startScore )
			break;
		else
		{
			startPos = objPos;
			startScore = objScore;
		}

		// Is it exceed search radius?
		if( !IsInCircle((int)m_bb.XMin(), (int)m_bb.YMin(), m_config.searchRadius, startPos) )
		{
			isSucceed = false;
			break;
		}
	}

	score = objScore;
	return objPos;
}

bool Tracker::IsInCircle(int centerX, int centerY, int radius, const IntRect& rect)
{
	double dis = (centerX - rect.XMin()) * (centerX - rect.XMin()) + (centerY - rect.YMin()) * (centerY - rect.YMin());
	return dis <= (radius * radius);
}
