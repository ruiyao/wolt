/* 
 * Struck: Structured Output Tracking with Kernels
 * 
 * Code to accompany the paper:
 *   Struck: Structured Output Tracking with Kernels
 *   Sam Hare, Amir Saffari, Philip H. S. Torr
 *   International Conference on Computer Vision (ICCV), 2011
 * 
 * Copyright (C) 2011 Sam Hare, Oxford Brookes University, Oxford, UK
 * 
 * This file is part of Struck.
 * 
 * Struck is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * Struck is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with Struck.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
 
#include "Tracker.h"
#include "Config.h"
#include "LaRank.h"

#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>

#include <opencv/cv.h>
#include <opencv/highgui.h>

#include <time.h>

using namespace std;
using namespace cv;

int drawing = 0;
cv::Rect box = cvRect(-1,-1,0,0);
IplImage* drawing_frame = NULL;

void rectangle(Mat& rMat, const FloatRect& rRect, const Scalar& rColour, int thickness)
{
	IntRect r(rRect);
	rectangle(rMat, Point(r.XMin(), r.YMin()), Point(r.XMax(), r.YMax()), rColour, thickness);
}

void draw_box( IplImage* img, CvRect rect ){
	cvRectangle( img, cvPoint(rect.x, rect.y), cvPoint(rect.x+rect.width,rect.y+rect.height),
				CV_RGB(255, 255, 255), 3 );
}

// Implement mouse callback
void on_mouse_callback( int event, int x, int y, int flags, void* param )
{
	switch( event )
	{
		case CV_EVENT_MOUSEMOVE: 
			if( drawing ){
				box.width = x-box.x;
				box.height = y-box.y;
			}
			break;

		case CV_EVENT_LBUTTONDOWN:
			drawing = true;
			box = cvRect( x, y, 0, 0 );
			break;

		case CV_EVENT_LBUTTONUP:
			drawing = false;
			if( box.width < 0 ){
				box.x += box.width;
				box.width *= -1;
			}
			if( box.height < 0 ){
				box.y += box.height;
				box.height *= -1;
			}
			draw_box( drawing_frame, box );
			break;
	}
}

int main(int argc, char* argv[])
{
	// read config file
	string configPath = "config.txt";
	if (argc > 1)
	{
		configPath = argv[1];
	}
	Config conf(configPath);
	cout << conf << endl;

	std::ostringstream n_greedSearchPoints;
	n_greedSearchPoints << conf.greedySearchPoints*2;
	
	if (conf.features.size() == 0)
	{
		cout << "error: no features specified in config" << endl;
		return EXIT_FAILURE;
	}
	
	std::fstream outFile;
	if (conf.resultsPath != "")
	{
		std::string resultPath = conf.sequenceBasePath+"/"+conf.sequenceName+"/WOLTracker_"+conf.sequenceName+conf.resultsPath;
		outFile.open(resultPath.c_str(), ios::out);
		if (!outFile)
		{
			cout << "error: could not open results file: " << conf.resultsPath << endl;
			return EXIT_FAILURE;
		}
	}

	// if no sequence specified then use the camera
	bool useCamera = (conf.sequenceName == "");
	int showWidth, showHeight;
	VideoCapture cap;
	
	int startFrame = -1;
	int endFrame = -1;
	FloatRect initBB;
	string imgFormat;
	float scaleW = 1.f;
	float scaleH = 1.f;

	const char* name = "WOLTracker v1.0";
	if (!conf.quietMode)
	{
		namedWindow(name);
	}
	
	if (useCamera)
	{
		if (!cap.open(0))
		{
			cout << "error: could not start camera capture" << endl;
			return EXIT_FAILURE;
		}
		startFrame = 0;
		endFrame = INT_MAX;
		Mat tmp;
		cap >> tmp;

		showWidth = tmp.cols;
		showHeight = tmp.rows;
		scaleW = (float)conf.frameWidth/showWidth;
		scaleH = (float)conf.frameHeight/showHeight;

		cvSetMouseCallback(name, &on_mouse_callback, 0);

		cout << "Draw a box, then press 'i' to initialise tracker" << endl;
	}
	else
	{
		// parse frames file
		string framesFilePath = conf.sequenceBasePath+"/"+conf.sequenceName+"/"+conf.sequenceName+"_frames.txt";
		ifstream framesFile(framesFilePath.c_str(), ios::in);
		if (!framesFile)
		{
			cout << "error: could not open sequence frames file: " << framesFilePath << endl;
			return EXIT_FAILURE;
		}
		string framesLine;
		getline(framesFile, framesLine);
		sscanf(framesLine.c_str(), "%d,%d", &startFrame, &endFrame);
		if (framesFile.fail() || startFrame == -1 || endFrame == -1)
		{
			cout << "error: could not parse sequence frames file" << endl;
			return EXIT_FAILURE;
		}
		
		imgFormat = conf.sequenceBasePath+"/"+conf.sequenceName+"/imgs/img%05d.jpg";
		
		// read first frame to get size
		char imgPath[256];
		sprintf(imgPath, imgFormat.c_str(), startFrame);
		Mat tmp = cv::imread(imgPath, 0);
		showWidth = conf.frameWidth;
		showHeight = conf.frameHeight;
		conf.frameWidth = tmp.cols;
		conf.frameHeight = tmp.rows;
		scaleW = (float)conf.frameWidth/tmp.cols;
		scaleH = (float)conf.frameHeight/tmp.rows;

		// read init box from ground truth file
		string gtFilePath = conf.sequenceBasePath+"/"+conf.sequenceName+"/"+conf.sequenceName+"_gt.txt";
		ifstream gtFile(gtFilePath.c_str(), ios::in);
		if (!gtFile)
		{
			cout << "error: could not open sequence gt file: " << gtFilePath << endl;
			return EXIT_FAILURE;
		}
		string gtLine;
		getline(gtFile, gtLine);
		float xmin = -1.f;
		float ymin = -1.f;
		float width = -1.f;
		float height = -1.f;
		sscanf(gtLine.c_str(), "%f,%f,%f,%f", &xmin, &ymin, &width, &height);
		if (gtFile.fail() || xmin < 0.f || ymin < 0.f || width < 0.f || height < 0.f)
		{
			cout << "error: could not parse sequence gt file" << endl;
			return EXIT_FAILURE;
		}
		initBB = FloatRect(xmin*scaleW, ymin*scaleH, width*scaleW, height*scaleH);
	}

	// save initial position, Ray
	if (outFile)
	{
		const FloatRect& bb = initBB;
		outFile << initBB.XMin()/scaleW << "," << initBB.YMin()/scaleH << "," << initBB.Width()/scaleW << "," << initBB.Height()/scaleH << endl;
	}
	
	Tracker tracker(conf);
	//if (!conf.quietMode)
	//{
	//	namedWindow("result");
	//}
	
	double totalTime = 0.0; // for compute time consuming
	Mat result(showHeight, showWidth, CV_8UC3);
	bool paused = false;
	bool doInitialise = false;
	bool isShowResult = true;
	for (int frameInd = startFrame; frameInd <= endFrame; ++frameInd)
	{
		unsigned t0=clock(); // for computing time
		Mat frame;
		Mat frameShowWin;
		if (useCamera)
		{
			Mat frameOrig;
			cap >> frameOrig;
			resize(frameOrig, frame, Size(conf.frameWidth, conf.frameHeight));
			flip(frame, frame, 1);
			resize(frameOrig, frameShowWin, Size(showWidth, showHeight));
			flip(frameShowWin, frameShowWin, 1);
			frameShowWin.copyTo(result);

			if (doInitialise)
			{
				if (tracker.IsInitialised())
				{
					tracker.Reset();
				}
				else
				{
					tracker.Initialise(frame, initBB);
				}
				doInitialise = false;
			}
			else if (!tracker.IsInitialised())
			{
				IplImage* frameShow = new IplImage(frameShowWin);
				if (!drawing_frame) // This frame is created only once
				{
					drawing_frame = cvCreateImage(cvSize(frameShow->width, frameShow->height), frameShow->depth, frameShow->nChannels);
					cvZero(drawing_frame);
				}

				for (int x = 0; x < frameShow->width; x++)
				{
					for (int y = 0; y < frameShow->height; y++)
					{
						CvScalar source = cvGet2D(frameShow, y, x);
						CvScalar over = cvGet2D(drawing_frame, y, x);

						CvScalar merged;
						CvScalar S = { 1,1,1,1 };
						CvScalar D = { 1,1,1,1 };

						for(int i = 0; i < 4; i++)
							merged.val[i] = (S.val[i] * source.val[i] + D.val[i] * over.val[i]);

						cvSet2D(frameShow, y, x, merged);
					}
				}

				initBB = FloatRect(box.x*scaleW, box.y*scaleW, box.width*scaleW, box.height*scaleW);

				isShowResult = false;
				imshow(name, frameShow);
			}
		}
		else
		{			
			char imgPath[256];
			sprintf(imgPath, imgFormat.c_str(), frameInd);
			Mat frameOrig = cv::imread(imgPath, 0);
			if (frameOrig.empty())
			{
				cout << "error: could not read frame: " << imgPath << endl;
				return EXIT_FAILURE;
			}
			resize(frameOrig, frame, Size(conf.frameWidth, conf.frameHeight));
			cvtColor(frame, result, CV_GRAY2RGB);
		
			if (frameInd == startFrame)
			{
				tracker.Initialise(frame, initBB);
			}
		}
		
		if (tracker.IsInitialised())
		{
			tracker.m_frmInd = frameInd;
			tracker.Track(frame);

			// output time consume, ray
			double frmTimeConsuming = (double)(clock() - t0)/CLOCKS_PER_SEC;
			totalTime += frmTimeConsuming;
			//std::cout << "Frame " << frameInd << " costs "<< frmTimeConsuming << " seconds."<< std::endl;
			
			if (!conf.quietMode && conf.debugMode)
			{
				tracker.Debug();
			}
			
			if (useCamera)
			{
				const FloatRect& sacledBB = FloatRect(tracker.GetBB().XMin()/scaleW, tracker.GetBB().YMin()/scaleW, 
														tracker.GetBB().Width()/scaleW, tracker.GetBB().Height()/scaleW);
				rectangle(result, sacledBB, CV_RGB(0, 255, 0), 2);
			}
			else
			{
				rectangle(result, tracker.GetBB(), CV_RGB(0, 255, 0), 2);
			}
			
			if (outFile)
			{
				const FloatRect& bb = tracker.GetBB();
				outFile << bb.XMin()/scaleW << "," << bb.YMin()/scaleH << "," << bb.Width()/scaleW << "," << bb.Height()/scaleH << endl;
			}

		}
		
		if (!conf.quietMode)
		{
			if (isShowResult)
			{
				imshow(name, result);
			}
			
			int key = waitKey(paused ? 0 : 1);
			if (key != -1)
			{
				if (key == 27 || key == 113) // esc q
				{
					break;
				}
				else if (key == 112) // p
				{
					paused = !paused;
				}
				else if (key == 105 && useCamera)
				{
					doInitialise = true;
					isShowResult = true;
				}
			}
			//if (conf.debugMode && frameInd == endFrame)
			//{
			//	cout << "\n\nend of sequence, press any key to exit" << endl;
			//	waitKey();
			//}
		}
		if (frameInd == endFrame)
		{
			// output time to file, Rui Yao, 2012-02-12
			//timeFile << totalTime/(endFrame-startFrame) << std::endl;

			std::cout << "Average Time Consuming: " << totalTime/endFrame << " seconds." << std::endl;
			cout << "\n\nend of sequence, press any key to exit" << endl;
			waitKey();
		}
	
	}
	
	if (outFile.is_open())
	{
		outFile.close();
	}
	//timeFile.close();
	
	return EXIT_SUCCESS;
}
